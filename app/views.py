# coding:utf-8

from flask import Flask, render_template, request, flash, session, redirect, url_for,g
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.googleauth import (GoogleFederated, GoogleAuth)
from app import app, auth, db
from models import *

# @admin.required
# @student.required


@app.before_first_request
def init():
    #db.drop_all() # 都合上起動する度にdbは削除. 本番環境に入れる前に消すこと！
    db.create_all()
    parse_xml()

@app.route('/')
@app.route('/index')
def index():
    """Return index.html
    """
    return render_template('index.html')

@app.route('/admin', methods=['GET', 'POST'])
@auth.required
def admin():
    """管理者画面
    """
    if is_email_adress_valid(g.user['email']) == True \
            and not get_school_id_from_mail_adress(g.user['email']) == u'e1020':
        flash(u'管理者以外ははいれません.')
        return redirect(url_for('home'))

    if request.method == 'POST':
        flash('Please enter all the fields.', 'error')
        return u'未実装'
        #return render_template('admin.html')
    else:
        return u'未実装'
        #return render_template('admin.html')

@app.route('/dailymenu', methods=['GET', 'POST'])
@auth.required
def dailymenu():
    """毎日メニュー(A, B)登録画面
    """
    if is_email_adress_valid(g.user['email']) == True \
            and not get_school_id_from_mail_adress(g.user['email']) == u'e1020':
        flash(u'管理者以外ははいれません.')
        return redirect(url_for('home'))

    if request.method == 'POST':
        # POST REQUEST
        if request.form['amenu'] == u'' \
                or request.form['bmenu'] == u'' \
                or request.form['date'] == u'':

            flash(u'入力項目が足りていません.', 'error')
            return render_template('dailymenu.html',
                weekdays = get_day_list())
        else:
            lunchs = db.session.query(DailyMenu).all()
            lunch = None
            for l in lunchs:
                if l.date.month == datetime.now().month\
                        and l.date.day == datetime.now().day:
                            lunch = l
                            break

            # 登録済みデータが無ければ普通に登録.
            if lunch == None:
                # 作成して,commit
                dailymenu = DailyMenu(request.form['amenu'],\
                            request.form['bmenu'],\
                            get_date_from_value(request.form['date']))
                db.session.add(dailymenu)
                db.session.commit()
                flash(u'毎日メニューを登録しました')
                return render_template('dailymenu.html',
                    weekdays = get_day_list())
            # すでに登録済みのデータがあれば更新.
            else:
                l.amenu = request.form['amenu']
                l.bmenu = request.form['bmenu']
                db.session.commit()
                flash(u'毎日メニューを更新しました')
                return render_template('dailymenu.html',
                    weekdays = get_day_list())

    else:
        return render_template('dailymenu.html',
                weekdays = get_day_list())

@app.route('/weeklymenu', methods=['GET', 'POST'])
@auth.required
def weeklymenu():
    """週替りメニュー(丼, ラーメン)登録画面
    """
    if is_email_adress_valid(g.user['email']) == True \
            and not get_school_id_from_mail_adress(g.user['email']) == u'e1020':
        flash(u'管理者以外ははいれません.')
        return redirect(url_for('home'))

    if request.method == 'POST':
        # POST REQUEST
        if request.form['don'] == u'' \
                or request.form['ramen'] == u'' \
                or request.form['start_date'] == u'':
            flash(u'入力項目が足りていません.', 'error')
            return render_template('weeklymenu.html',
                mondays = get_week_list())
        else:
            lunchs = db.session.query(WeeklyMenu).all()
            lunch = None
            for l in lunchs:
                if l.start_date.month == get_monday_date().month\
                        and l.start_date.day == get_monday_date().day:
                            lunch = l
                            break

            # 登録済みデータが無ければ普通に登録.
            if lunch == None:
                # 作成して,commit
                weeklymenu = WeeklyMenu(request.form['don'],\
                        request.form['ramen'],\
                        get_date_from_value(request.form['start_date']))
                db.session.add(weeklymenu)
                db.session.commit()
                flash(u'週替りメニューを登録しました')
                return render_template('weeklymenu.html',
                    mondays = get_week_list())
            # すでに登録済みのデータがあれば更新.
            else:
                l.don   = request.form['don']
                l.ramen = request.form['ramen']
                db.session.commit()
                flash(u'週替りメニューを更新しました')
                return render_template('weeklymenu.html',
                    weekdays = get_day_list())

    else:
        return render_template('weeklymenu.html',
                mondays = get_week_list())

@app.route('/create_account', methods=['GET', 'POST'])
@auth.required
def create_account():
    """create a user account
    """

    # 先生はadminにとばす.
    if is_email_adress_valid(g.user['email']) == False:
        flash('学生でない方は、アカウントを作成できません.')
        return redirect(url_for('admin'))


    if request.method == 'POST':

        # POST REQUEST
        if request.form['grade'] == u'' \
                or request.form['department'] == u'':

            flash(u'入力項目が足りていません.', 'error')
            return render_template('create_account.html')
        else:

            if request.form.get('sex') == u'female':
                sex = True
            else:
                sex = False
            if request.form.get('abroad') is None:
                abroad = False
            else:
                abroad = True

            get_department = request.form['department']
            department = 1 # ミスってた時のためにE科で初期化.
            if get_department == u'M':
                department = 0
            elif get_department == u'E':
                department = 1
            elif get_department == u'C':
                department = 2
            elif get_department == u'A':
                department = 3
            elif get_department == u'ME':
                department = 4
            elif get_department == u'AC':
                department = 5

            grade = int(request.form['grade'])

            if grade == 4 and department == 1\
                    or grade == 5 and department == 1:
                if request.form.get('course') == u'electoric':
                    course = True
                else:
                    course = False
            else:
                course = None

            user = User(get_school_id_from_mail_adress(g.user['email']),
                        grade,
                        department,
                        course,
                        sex,
                        abroad)

            db.session.add(user)
            db.session.commit()
            flash('Your account was successfully created')
            return redirect(url_for('home'))
    else:
        # GET REQUEST
        user_info = predict_user_info(g.user['email'])
        # userinfo["department"] or userinfo["grade"]みたいに使う.
        return render_template('create_account.html',
                userinfo = user_info)


@app.route('/home')
@auth.required
def home():
    """
    Show TimeTable & Lunch Menu
    """

    # 先生はadminにとばす.
    if is_email_adress_valid(g.user['email']) == False:
        flash('学生でない方は、アカウントを作成できません.')
        return redirect(url_for('admin'))

    user = db.session.query(User).filter(User.school_id == \
            get_school_id_from_mail_adress(g.user['email'])).first()

    if user == None:
        flash(u'アカウントを作成して下さい.')
        return redirect(url_for('create_account'))

    # 管理画面表示フラグ.
    admin_flag = True
    if is_email_adress_valid(g.user['email']) == True \
            and not get_school_id_from_mail_adress(g.user['email']) == u'e1020':
        admin_flag = False

    # 献立
    aset  = None
    bset  = None
    don   = None
    ramen = None
    lunchs = db.session.query(DailyMenu).all()
    for lunch in lunchs:
        if lunch.date.month == datetime.now().month\
                and lunch.date.day == datetime.now().day:
                    aset = lunch.amenu
                    bset = lunch.bmenu
                    break
    weeks = db.session.query(WeeklyMenu).all()
    for week in weeks:
        if week.start_date.month == get_monday_date().month\
                and week.start_date.day == get_monday_date().day:
                    don   = week.don
                    ramen = week.ramen
                    break
    school_lunch = {"aset":aset,"bset":bset, "don":don, "ramen":ramen}

    # 授業一覧取得.
    classes = db.session.query(GradeTable).filter(GradeTable.grade == user.grade)\
            .filter(GradeTable.department == user.department)\
            .filter(GradeTable.wday == get_today_wday())\
            .order_by(GradeTable.starttime)\
            .all()

    # スライスを使って一時的なコピーを作る.
    for cl in classes[:]:
        if cl.sex != None and cl.sex != user.sex:
            classes.remove(cl)
        elif cl.abroad != None and cl.abroad != user.abroad:
            classes.remove(cl)
        elif user.course != None and cl.course != user.course:
            classes.remove(cl)

    return render_template('home.html',
            timetables = classes,
            lunchs = school_lunch,
            admin  = admin_flag,
            weekday = get_today_wday())

@app.route('/login_route')
def login_route():
    """
    Login. homeにauth.requiredつけてるけど、
    そこでflashつけるとホームに戻る度に
    ログイン成功メッセージが表示されてしまう.
    """
    auth._login()
    #flash(u'ログインに成功しました. ようこそ！%s %sさん' % (g.user['last_name'], g.user['first_name']) )
    #flash(u'Success to login! Welcome!' )
    return redirect(url_for('home'))

@app.route('/logout_route')
def logout_route():
    """Logout"""
    auth._logout()
    flash(u'ログアウトしました.' )
    return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return render_template('404.html')

@app.errorhandler(500)
def error_occured(e):
    """Return a custom 500 error."""
    return render_template('error.html')
