# coding: utf-8
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, DAILY, MO, TU, WE, TH, FR

def get_date_from_value(value):
    """get datetime object created form value.
    """
    datelist = value.split("-")
    return datetime(int(datelist[0]), int(datelist[1]), int(datelist[2]))

def get_today_wday():
    """get today's weekday
    """
    return datetime.now().weekday()+1

def get_day_list():
    """get week day list.
    """
    rrule_obj = rrule(DAILY,
        byweekday=(MO,TU,WE,TH,FR),
        dtstart=datetime.now(),
        until=datetime.now() + relativedelta(days=30)
        )

    weekdays = []

    for obj in rrule_obj:
        weekdays.append({"year":obj.year,"month":obj.month,"day":obj.day})

    return weekdays

def get_week_list():
    """get monday dates list.
    """
    rrule_obj = rrule(DAILY,
        byweekday=(MO),
        dtstart=datetime.now() - relativedelta(days=6),
        until=datetime.now() + relativedelta(months=2)
        )

    mondays = []

    for obj in rrule_obj:
        mondays.append({"start_year":obj.year,"start_month":obj.month,"start_day":obj.day,"end_month":(obj+relativedelta(days=4)).month,"end_day":(obj+relativedelta(days=4)).day})

    return mondays

def get_monday_date():
    """get monday of this week.
    """
    rrule_obj = rrule(DAILY,
        byweekday=(MO),
        dtstart=datetime.now() - relativedelta(days=6),
        until=datetime.now()
        )
    return rrule_obj[0]

if __name__ == '__main__':
    import doctest
    doctest.testmod()
