# coding:utf-8

import urllib2
from xml.etree.ElementTree import *
from app import db

def get_xml_string(TIMETABLE_URL):
    try:
        xmlString = urllib2.urlopen(TIMETABLE_URL).read()
    except urllib2.HTTPError as err:
        print('HTTPError')
        print(err)
    except urllib2.URLError as err:
        print('URLError')
        print(err)
        if isinstance(err.reason, socket.timeout):
            print('timeout')
    else:
        print('Get XML')

    return fromstring(xmlString)

def parse_xml():
    TIMETABLE_URL = "http://www.akashi.ac.jp/data/timetable/timetable201404.xml"
    element = get_xml_string(TIMETABLE_URL)

    for elem1 in element[0][1]:
        timetable = GradeTable()
        for elem in elem1:
            if 'Name' in elem.tag:
                timetable.name = elem.text
            elif 'Grade' in elem.tag:
                timetable.grade = int(elem.text)
            elif 'Department' in elem.tag:
                if elem.text == u'機械工学科':
                    timetable.department = 0
                elif elem.text == u'電気情報工学科':
                    timetable.department = 1
                elif elem.text == u'都市システム工学科':
                    timetable.department = 2
                elif elem.text == u'建築学科':
                    timetable.department = 3
            elif 'Course' in elem.tag:
                if elem.text == u'電気電子工学コース':
                    timetable.course = True
                elif elem.text == u'情報工学コース':
                    timetable.course = False
            elif 'Wday' in elem.tag:
                timetable.wday = int(elem.text)
            elif 'Uri' in elem.tag:
                timetable.syllabus = elem.text
            elif 'StartTime' in elem.tag:
                #print elem.text[:8]
                if '09:00:00' == elem.text[:8]:
                    timetable.starttime = 1
                elif '10:40:00' == elem.text[:8]:
                    timetable.starttime = 2
                elif '13:00:00' == elem.text[:8]:
                    timetable.starttime = 3
                elif '14:40:00' == elem.text[:8]:
                    timetable.starttime = 4
            elif 'EndTime' in elem.tag:
                #print elem.text[:8]
                if '10:30:00' == elem.text[:8]:
                    timetable.endtime = 1
                elif '12:10:00' == elem.text[:8]:
                    timetable.endtime = 2
                elif '14:30:00' == elem.text[:8]:
                    timetable.endtime = 3
                elif '16:10:00' == elem.text[:8]:
                    timetable.endtime = 4
        if u'女子' in timetable.name:
            timetable.sex = True
        if u'男子' in timetable.name:
            timetable.sex = False
        if u'日本語' in timetable.name:
            timetable.abroad = True
        db.session.add(timetable)
    db.session.commit()

class User(db.Model):
    """user table"""
    __tablename__ = 'users'
    id         = db.Column('student_id', db.Integer, primary_key=True)
    school_id  = db.Column(db.String(16))
    grade      = db.Column(db.Integer)
    department = db.Column(db.Integer) # 0:m, 1:e, 2:c, 3:a, 4:me ,5:ac
    course     = db.Column(db.Boolean) # true: e, false:j
    sex        = db.Column(db.Boolean) # true: women only, false: men only
    abroad     = db.Column(db.Boolean) # true: abroad only, false:japanese only

    def __init__(self, school_id, grade, department, course, sex, abroad):
        """Initializes the fields with entered data.
        """
        self.school_id  = school_id
        self.grade      = grade
        self.department = department
        self.course     = course
        self.sex        = sex
        self.abroad     = abroad

    def __repr__(self):
        return '<User %r>' % self.school_id

class WeeklyMenu(db.Model):
    """week menu table.
    """
    __tablename__ = 'weeklymenus'
    week_id    = db.Column(db.Integer, primary_key=True)
    don        = db.Column(db.String(32))
    ramen      = db.Column(db.String(32))
    start_date = db.Column(db.DateTime)

    def __init__(self, don, ramen, start_date):
        """Initializes the fields with entered data
        """
        self.don        = don
        self.ramen      = ramen
        self.start_date = start_date

    def __repr__(self):
        return '<WeeklyTable %r>' % (self.start_date)

class DailyMenu(db.Model):
    """dayly menu table.
    """
    __tablename__ = 'dailymenus'
    id     = db.Column(db.Integer, primary_key=True)
    amenu  = db.Column(db.String(32))
    bmenu  = db.Column(db.String(32))
    date   = db.Column(db.DateTime)

    def __init__(self, amenu, bmenu, date):
        """ Initializes the fields with entered data """
        self.amenu = amenu
        self.bmenu = bmenu
        self.date  = date

    def __repr__(self):
        return '<DailyTable %r>' % (self.date)

class GradeTable(db.Model):
    """grade table.
    """
    __tablename__ = 'gradetables'
    id  = db.Column(db.Integer, primary_key=True)
    grade      = db.Column(db.Integer)
    department = db.Column(db.Integer) # 0:m, 1:e, 2:c, 3:a, 4:me ,5:ac
    course     = db.Column(db.Boolean) # true: e, false:j
    wday       = db.Column(db.Integer) # 0:mon, 1:tue, ...
    sex        = db.Column(db.Boolean) # true: women only, false: men only
    abroad     = db.Column(db.Boolean) # true: abroad only, false:japanese only
    name       = db.Column(db.String(64))
    syllabus   = db.Column(db.String(64))
    starttime  = db.Column(db.Integer) # 1-4
    endtime    = db.Column(db.Integer) # 1-4

    def __repr__(self):
        return '<GradeTable %r %r>' % (self.grade,self.department)
